Extracting MetaData information...
Having found CutFlow for Data, printing...
CutFlowHisto                                DSID_351364_CutFlow
Systematic variation                                  
###############################################################
Cut                                         Yields    
###############################################################
Initial                                     25798 ± 160.62
PassGRL                                     25798 ± 160.62
passLArTile                                 25764 ± 160.51
Trigger                                     4796  ± 69.25
HasVtx                                      4796  ± 69.25
BadJet                                      4796  ± 69.25
CosmicMuon                                  4702  ± 68.57
BadMuon                                     4702  ± 68.57
>= 2 jets                                   3221  ± 56.75
>= 1 b-jet                                  1689  ± 41.10
(Muon pt > 50 GeV || Electron pt > 50 GeV)  138   ± 11.75
MetTST > 50 GeV                             68    ± 8.25
Final                                       68    ± 8.25
###############################################################
