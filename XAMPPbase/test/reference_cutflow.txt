Extracting MetaData information...
Having found CutFlow for MC, printing...
Printing raw events
CutFlowHisto                                DSID_410472_CutFlow
Systematic variation                        Nominal   
###############################################################
Cut                                         Yields    
###############################################################
Initial                                     79370 ± 281.73
PassGRL                                     79370 ± 281.73
passLArTile                                 79370 ± 281.73
Trigger                                     36223 ± 190.32
HasVtx                                      36223 ± 190.32
BadJet                                      36223 ± 190.32
CosmicMuon                                  35799 ± 189.21
BadMuon                                     35795 ± 189.20
>= 2 jets                                   34011 ± 184.42
>= 1 b-jet                                  30326 ± 174.14
(Muon pt > 50 GeV || Electron pt > 50 GeV)  19107 ± 138.23
MetTST > 50 GeV                             13698 ± 117.04
Final                                       13698 ± 117.04
###############################################################
