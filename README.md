[![pipeline status](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPbase/badges/master/pipeline.svg)](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPbase/commits/master)

Welcome to XAMPP! 
===
    
Introduction 
---

The XAMPPbase package can be used to run an analysis on ATLAS xAODs (original and derived). An analysis and its selections have to be defined. The standard output is a root file with cutflow histograms and trees for each systematic uncertainty affecting the event kinematics and each analysis. Additional analysis specific classes can be implemented in your own analysis package inheriting from XAMPPbase. SUSYTools is exploited for the implementation of the CP recommendations in all classes containing *SUSY* in their name. You can also implement your own CP tools independent of SUSYTools if needed. For further information please visit our Twiki https://twiki.cern.ch/twiki/bin/view/AtlasProtected/XAMPPSoftwareFramework.

Setup the package
---

We recommend not to work on the master branch, but instead creating your own branch and using Merge Requests through the gitlab web interface to push your changes to the master branch. You can create your branch by clicking on 'Branches' and choosing 'New branch'. If you prefer working in your own fork, feel free to create one from this repository.


    


```
mkdir build run
git clone ssh://git@gitlab.cern.ch:7999/atlas-mpp-xampp/XAMPPbase.git --recursive source -b <your branch / master>
cd source
asetup AthAnalysis,21.2.103,here
cd ../build && cmake ../source && make
cd ../run && source ../build/${AthAnalysis_PLATFORM}/setup.sh
```

Running the code
---

In order to run on the first 1000 events of a single input file without systematics, please use

```
python XAMPPbase/python/runAthena.py --jobOptions XAMPPbase/runXAMPPbase.py --filesInput <InFile> --evtMax 1000
```

For more possible options, use:

```
python XAMPPbase/python/runAthena.py -h
```


Submitting to the grid
---

In order to submit your code to the grid, use:

```
python XAMPPbase/python/SubmitToGrid.py --jobOptions XAMPPbase/runXAMPPbase.py --inputDS <dataset name> --outDS <name of grid output container>
```
Please be aware that the grid is currently in a hybrid phase between `Centos7` and the good old `Centos6` bringing lot's of troubles for the endusers if they submit from a `Centos7` machine. The current recommendation is to setup the code inside a singularity container, compile it and submit it then from there. Singularity can be setup very easily inside the `ATLAS` software just type inside your home:
```
setupATLASUI -c sl6+batch
```


How to run without processing systematic uncertainites
---

It is possible to run without variations to account for systematic uncertainties. Since only the TTree accounting for the nominal value is written out this speeds up your jobs considerably, which might be useful for testing. 
Disabling systematics is possible using the command line option `--noSyst`:

```
python XAMPPbase/python/runAthena.py --jobOptions XAMPPbase/runXAMPPbase.py --inFile <InFile> --noSyst
```

How to monitor performance using valgrind
---

```
lsetup "lcgenv -p LCG_95 x86_64-centos7-gcc7-opt valgrind"
valgrind --suppressions=${ROOTSYS}/etc/valgrind-root.supp  --tool=callgrind --smc-check=all --num-callers=50  --trace-children=yes athena --filesInput /ptmp/mpp/junggjo9/Datasets/mc16_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.deriv.DAOD_SUSY18.e3601_s3126_r9364_p3636/DAOD_SUSY18.15430984._000016.pool.root.1  --evtMax 1000 XAMPPbase/runXAMPPbase.py 
```

You can also use the built-in command-line arguments to launch the valgrind tools (memcheck, callgrind, massif):
```
python XAMPPbase/python/runAthena.py --valgrind {memcheck, callgrind, massif} [other arguments]
```


Using the latest GRL
---

The latest GRL and ilumicalc file are directly loaded from the GoodRunLists grouparea. You can find the current configuration in the project `ClusterSubmission`, where the GRL information is stored for all XAMPP projects

https://gitlab.cern.ch/atlas-mpp-xampp/ClusterSubmission/blob/master/data/GRL.json


Doxygen documentation
---

A doxygen documentation of the package can be found at:

https://xamppbase-doxygen.web.cern.ch/

```
**********************************************
**********************************************
***                                        ***
***  XAMPPbase - (c) 2015-2019             ***
***  Developers:                           ***
***      jojungge         @ cern.ch        ***
***      nicolas.koehler  @ cern.ch        ***
***      pgadow           @ cern.ch        ***
***      goblirsc         @ cern.ch        ***
***                                        ***
**********************************************
**********************************************
```
